cmake_minimum_required(VERSION 3.13)

# project information
project(unfolder CXX)

# Source folder
add_subdirectory(src)
