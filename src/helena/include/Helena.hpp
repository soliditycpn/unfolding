#ifndef HELENA_H_
#define HELENA_H_

#include <stddef.h>

#include <list>
#include <memory>
#include <sstream>
#include <string>
#include <vector>

namespace HELENA {

/**
 * Declare a list of reserved string
 */
const std::string ASSERT_TOKEN = "assert";
const std::string AND_TOKEN = "and";
const std::string CAPACITY_TOKEN = "capacity";
const std::string CARD_TOKEN = "card";
const std::string CASE_TOKEN = "case";
const std::string CONSTANT_TOKEN = "constant";
const std::string DEFAULT_TOKEN = "default";
const std::string DESCRIPTION_TOKEN = "description";
const std::string DOM_TOKEN = "dom";
const std::string ELSE_TOKEN = "else";
const std::string EMPTY_TOKEN = "empty";
const std::string ENUM_TOKEN = "enum";
const std::string EPSILON_TOKEN = "epsilon";
const std::string EXISTS_TOKEN = "exists";
const std::string FOR_TOKEN = "for";
const std::string FORALL_TOKEN = "forall";
const std::string FUNCTION_TOKEN = "function";
const std::string GUARD_TOKEN = "guard";
const std::string IF_TOKEN = "if";
const std::string IMPORT_TOKEN = "import";
const std::string IN_TOKEN = "in";
const std::string INIT_TOKEN = "init";
const std::string INHIBIT_TOKEN = "inhibit";
const std::string LET_TOKEN = "let";
const std::string LIST_TOKEN = "list";
const std::string MAX_TOKEN = "max";
const std::string MIN_TOKEN = "min";
const std::string MOD_TOKEN = "mod";
const std::string MULT_TOKEN = "mult";
const std::string NOT_TOKEN = "not";
const std::string OF_TOKEN = "of";
const std::string OR_TOKEN = "or";
const std::string OUT_TOKEN = "out";
const std::string PICK_TOKEN = "pick";
const std::string PLACE_TOKEN = "place";
const std::string PRED_TOKEN = "pred";
const std::string PRINT_TOKEN = "print";
const std::string PRIORITY_TOKEN = "priority";
const std::string PROPOSITION_TOKEN = "proposition";
const std::string PRODUCT_TOKEN = "product";
const std::string RANGE_TOKEN = "range";
const std::string RETURN_TOKEN = "return";
const std::string STRUCT_TOKEN = "struct";
const std::string SAFE_TOKEN = "safe";
const std::string SET_TOKEN = "set";
const std::string SUBTYPE_TOKEN = "subtype";
const std::string SUCC_TOKEN = "succ";
const std::string SUM_TOKEN = "sum";
const std::string TRANSITION_TOKEN = "transition";
const std::string TYPE_TOKEN = "type";
const std::string VECTOR_TOKEN = "vector";
const std::string WHILE_TOKEN = "while";
const std::string WITH_TOKEN = "with";
const std::list<std::string> ReservedTokensList{
    ASSERT_TOKEN,     AND_TOKEN,      CAPACITY_TOKEN,    CARD_TOKEN,
    CASE_TOKEN,       CONSTANT_TOKEN, DEFAULT_TOKEN,     DESCRIPTION_TOKEN,
    DOM_TOKEN,        ELSE_TOKEN,     EMPTY_TOKEN,       ENUM_TOKEN,
    EPSILON_TOKEN,    EXISTS_TOKEN,   FOR_TOKEN,         FORALL_TOKEN,
    FUNCTION_TOKEN,   GUARD_TOKEN,    IF_TOKEN,          IMPORT_TOKEN,
    IN_TOKEN,         INIT_TOKEN,     INHIBIT_TOKEN,     LET_TOKEN,
    LIST_TOKEN,       MAX_TOKEN,      MIN_TOKEN,         MOD_TOKEN,
    MULT_TOKEN,       NOT_TOKEN,      OF_TOKEN,          OR_TOKEN,
    OUT_TOKEN,        PICK_TOKEN,     PLACE_TOKEN,       PRED_TOKEN,
    PRINT_TOKEN,      PRIORITY_TOKEN, PROPOSITION_TOKEN, PRODUCT_TOKEN,
    RANGE_TOKEN,      RETURN_TOKEN,   STRUCT_TOKEN,      SAFE_TOKEN,
    SET_TOKEN,        SUBTYPE_TOKEN,  SUCC_TOKEN,        SUM_TOKEN,
    TRANSITION_TOKEN, TYPE_TOKEN,     VECTOR_TOKEN,      WHILE_TOKEN,
    WITH_TOKEN};

/**
 * Declare a net
 */
const std::string Net_Token = "Net";

/**
 * Declare parameters
 */
const std::string Net_Param_Token = "Net_Param";

/**
 * Declare colors
 */
const std::string Color_Token = "Color";
const std::string Range_Color_Token = "Range_Color";
const std::string Mod_Color_Token = "Mod_Color";
const std::string Enum_Color_Token = "Enum_Color";
const std::string Vector_Color_Token = "Vector_Color";
const std::string Struct_Color_Token = "Struct_Color";
const std::string Component_Token = "Component";
const std::string List_Color_Token = "List_Color";
const std::string Set_Color_Token = "Set_Color";
const std::string Sub_Color_Token = "Sub_Color";

/**
 * Declare functions
 */
const std::string Func_Prot_Token = "Func_Prot";
const std::string Func_Token = "Func";
const std::string Param_Token = "Param";
const std::string Var_Decl_Token = "Var_Decl";

/**
 * Declare the expressions
 */
const std::string Num_Const_Token = "Num_Const";
const std::string Func_Call_Token = "Func_Call";
const std::string Vector_Access_Token = "Vector_Access";
const std::string Struct_Access_Token = "Struct_Access";
const std::string Bin_Op_Token = "Bin_Op";
const std::string Un_Op_Token = "Un_Op";
const std::string Vector_Aggregate_Token = "Vector_Aggregate";
const std::string Vector_Assign_Token = "Vector_Assign";
const std::string Struct_Aggregate_Token = "Struct_Aggregate";
const std::string Struct_Assign_Token = "Struct_Assign";
const std::string Symbol_Token = "Symbol";
const std::string Iterator_Token = "Iterator";
const std::string Tuple_Access_Token = "Tuple_Access";
const std::string Attribute_Token = "Attribute";
const std::string Container_Aggregate_Token = "Container_Aggregate";
const std::string Empty_Token = "Empty";
const std::string List_Slice_Token = "List_Slice";

/**
 * Declare iterator types
 */
const std::string card_iterator_Token = "card_iterator";
const std::string mult_iterator_Token = "mult_iterator";
const std::string forall_iterator_Token = "forall_iterator";
const std::string exists_iterator_Token = "exists_iterator";
const std::string max_iterator_Token = "max_iterator";
const std::string min_iterator_Token = "min_iterator";
const std::string sum_iterator_Token = "sum_iterator";
const std::string product_iterator_Token = "product_iterator";

/**
 * Declare unary operators
 */
const std::string Pred_Op_Token = "Pred_Op";
const std::string Succ_Op_Token = "Succ_Op";
const std::string Plus_Op_Token = "Plus_Op";
const std::string Minus_Op_Token = "Minus_Op";
const std::string Not_Op_Token = "Not_Op";

/**
 * Declare binary operators
 */
const std::string Mult_Op_Token = "Mult_Op";
const std::string Div_Op_Token = "Div_Op";
const std::string Mod_Op_Token = "Mod_Op";
const std::string And_Op_Token = "And_Op";
const std::string Or_Op_Token = "Or_Op";
const std::string Sup_Op_Token = "Sup_Op";
const std::string Sup_Eq_Op_Token = "Sup_Eq_Op";
const std::string Inf_Op_Token = "Inf_Op";
const std::string Inf_Eq_Op_Token = "Inf_Eq_Op";
const std::string Eq_Op_Token = "Eq_Op";
const std::string Neq_Op_Token = "Neq_Op";
const std::string Amp_Op_Token = "Amp_Op";
const std::string In_Op_Token = "In_Op";

/**
 * Declare the statements
 */
const std::string Assign_Token = "Assign";
const std::string If_Then_Else_Token = "If_Then_Else";
const std::string Case_Stat_Token = "Case_Stat";
const std::string Case_Alternative_Token = "Case_Alternative";
const std::string While_Stat_Token = "While_Stat";
const std::string Print_Stat_Token = "Print_Stat";
const std::string Return_Stat_Token = "Return_Stat";
const std::string For_Stat_Token = "For_Stat";
const std::string Block_Stat_Token = "Block_Stat";

/**
 * Declare the places
 */
const std::string Place_Token = "Place";
const std::string Place_Init_Token = "Place_Init";
const std::string Place_Capacity_Token = "Place_Capacity";
const std::string Place_Type_Token = "Place_Type";

/**
 * Declare the transitions
 */
const std::string Transition_Token = "Transition";
const std::string Transition_Description_Token = "Transition_Description";
const std::string Transition_Guard_Token = "Transition_Guard";
const std::string Transition_Priority_Token = "Transition_Priority";
const std::string Transition_Safe_Token = "Transition_Safe";

/**
 * Declare the mappings
 */
const std::string Arc_Token = "Arc";
const std::string Mapping_Token = "Mapping";
const std::string Tuple_Token = "Tuple";
const std::string Simple_Tuple_Token = "Simple_Tuple";

/**
 * Declare the propositions
 */
const std::string Proposition_Token = "Proposition";

/**
 * Declare the others
 */
const std::string Assert_Token = "Assert";
const std::string Iter_Variable_Token = "Iter_Variable";
const std::string Low_High_Range_Token = "Low_High_Range";
const std::string Name_Token = "Name";
const std::string A_String_Token = "A_String";
const std::string Number_Token = "Number";
const std::string List_Token = "List";

/**
 * Definition of the node types
 */
enum LnaNodeType {
  LnaNodeTypeNet,
  LnaNodeTypeNet_Param,
  LnaNodeTypeComment,
  LnaNodeTypeColor,
  LnaNodeTypeRange_Color,
  LnaNodeTypeMod_Color,
  LnaNodeTypeEnum_Color,
  LnaNodeTypeVector_Color,
  LnaNodeTypeStruct_Color,
  LnaNodeTypeComponent,
  LnaNodeTypeListColor,
  LnaNodeTypeSet_Color,
  LnaNodeTypeSub_Color,
  //
  LnaNodeTypeConstant,
  //
  LnaNodeTypeFunc_Prot,
  LnaNodeTypeFunc,
  LnaNodeTypeParam,
  LnaNodeTypeVar_Decl,
  LnaNodeTypeNum_Const,
  LnaNodeTypeFunc_Call,
  LnaNodeTypeVector_Access,
  LnaNodeTypeStruct_Access,
  LnaNodeTypeBin_Op,
  LnaNodeTypeUn_Op,
  LnaNodeTypeVector_Aggregate,
  LnaNodeTypeVector_Assign,
  LnaNodeTypeStruct_Aggregate,
  LnaNodeTypeStruct_Assign,
  LnaNodeTypeSymbol,
  LnaNodeTypeIterator,
  LnaNodeTypeTuple_Access,
  LnaNodeTypeAttribute,
  LnaNodeTypeContainer_Aggregate,
  LnaNodeTypeEmpty,
  LnaNodeTypeList_Slice,
  LnaNodeTypecard_iterator,
  LnaNodeTypemult_iterator,
  LnaNodeTypeforall_iterator,
  LnaNodeTypeexists_iterator,
  LnaNodeTypemax_iterator,
  LnaNodeTypemin_iterator,
  LnaNodeTypesum_iterator,
  LnaNodeTypeproduct_iterator,
  LnaNodeTypePred_Op,
  LnaNodeTypeSucc_Op,
  LnaNodeTypePlus_Op,
  LnaNodeTypeMinus_Op,
  LnaNodeTypeNot_Op,
  LnaNodeTypeMult_Op,
  LnaNodeTypeDiv_Op,
  LnaNodeTypeMod_Op,
  LnaNodeTypeAnd_Op,
  LnaNodeTypeOr_Op,
  LnaNodeTypeSup_Op,
  LnaNodeTypeSup_Eq_Op,
  LnaNodeTypeInf_Op,
  LnaNodeTypeInf_Eq_Op,
  LnaNodeTypeEq_Op,
  LnaNodeTypeNeq_Op,
  LnaNodeTypeAmp_Op,
  LnaNodeTypeIn_Op,
  LnaNodeTypeAssign,
  LnaNodeTypeIf_Then_Else,
  LnaNodeTypeCase_Stat,
  LnaNodeTypeCase_Alternative,
  LnaNodeTypeWhile_Stat,
  LnaNodeTypePrint_Stat,
  LnaNodeTypeReturn_Stat,
  LnaNodeTypeFor_Stat,
  LnaNodeTypeBlock_Stat,
  LnaNodeTypePlace,
  LnaNodeTypePlace_Init,
  LnaNodeTypePlace_Capacity,
  LnaNodeTypePlace_Type,
  LnaNodeTypeTransition,
  LnaNodeTypeTransition_Description,
  LnaNodeTypeTransition_Guard,
  LnaNodeTypeTransition_Priority,
  LnaNodeTypeTransition_Safe,
  LnaNodeTypeArc,
  LnaNodeTypeMapping,
  LnaNodeTypeTuple,
  LnaNodeTypeSimple_Tuple,
  LnaNodeTypeProposition,
  LnaNodeTypeAssert,
  LnaNodeTypeIter_Variable,
  LnaNodeTypeLow_High_Range,
  LnaNodeTypeName,
  LnaNodeTypeA_String,
  LnaNodeTypeNumber,
  LnaNodeTypeList,
  LnaNodeTypeSubNet,
  LnaNodeTypeStructuredNet
};

class LnaNode;

/**
 * Definition of the type for pointers of LNA nodes
 */
typedef std::shared_ptr<LnaNode> LnaNodePtr;

/**
 * Abstract class defining a Helena element
 */
class LnaNode {
 public:
  /**
   * Create a new node
   * @param _node_type type of the new node
   */
  explicit LnaNode(LnaNodeType _node_type) : node_type(_node_type) {}

  /**
   * Get the type of the node
   *
   * @return type
   */
  LnaNodeType get_node_type() const;

  /**
   * Get the size of the node
   *
   * @return size
   */
  size_t size() const;

  /**
   * Return the Helena code of the node
   *
   * @return Helena code
   */
  virtual std::string source_code() = 0;

 protected:
  /**
   * Add subnode to the collection
   *
   * @param _node node to be added
   */
  void append_sub_node(const LnaNodePtr& _node);

  /**
   * Delete subnode from the collection
   *
   * @param x identifier of the subnode
   */
  void delete_sub_node(const unsigned int& x);

  /**
   * update the subnode
   *
   * @param x identifier of the subnode
   * @param _node new subnode
   */
  void update_sub_node(const unsigned int& x, const LnaNodePtr _node);

  /**
   * Get a subnode
   *
   * @param x identifier of the subnode
   * @return subnode
   */
  LnaNodePtr get_sub_node(const unsigned int& x) const;

  LnaNodeType node_type;
  std::vector<LnaNodePtr> lna_nodes;
};

/**
 * @brief Class representing a net parameter
 *
 * A net may have parameters such as e.g., a number of processes. They are
 * interpreted as constants of the predefined int type. The advantage of using
 * parameters is that their values can be changed via the command line when
 * helena is invoked, i.e., without changing the model file.
 */
class ParameterNode : public LnaNode {
 public:
  /**
   * Create a new net parameter
   */
  ParameterNode() : LnaNode(LnaNodeTypeNet_Param) {}

  /**
   * Return the Helena code of the net parameter
   *
   * @return Helena code
   */
  std::string source_code();

  /**
   * Set the name of the net parameter
   *
   * @param _name new name
   */
  void set_name(const std::string& _name);

  /**
   * Get the name of the net parameter
   *
   * @return name
   */
  std::string get_name() const;

  /**
   * Set the value of the net parameter
   *
   * @param _number new parameter value
   */
  void set_number(const std::string& _number);

  /**
   * Get the value of the net parameter
   *
   * @return value of the net parameter
   */
  std::string get_number() const;

 private:
  /**
   * name of the parameter node
   */
  std::string name;

  /**
   * number of the parameter node
   */
  std::string number;
};

/**
 * Type of pointers for ParameterNodes
 */
typedef std::shared_ptr<ParameterNode> ParameterNodePtr;

/**
 * @brief Class representing a net
 *
 * A net is described by a list of definitions that are the components of the
 * net. Elements that may be defined in a net are date types, constants,
 * functions, places, transitions, and state propositions
 */
class NetNode : public LnaNode {
 public:
  /**
   * Create a new net
   */
  NetNode() : LnaNode(LnaNodeTypeNet) {}

  /**
   * Create a new net
   *
   * @param _name  name of the net
   */
  NetNode(std::string _name) : LnaNode(LnaNodeTypeNet), name(_name) {}

  /**
   * Return the Helena code of the net
   *
   * @return helena code
   */
  std::string source_code();

  /**
   * Set the name of the net
   *
   * @param _name new name
   */
  void set_name(const std::string& _name);

  /**
   * Get the name of the net
   *
   * @return net's name
   */
  std::string get_name() const;

  /**
   * Add a net parameter
   *
   * @param _node new net parameter
   */
  void add_parameter(const ParameterNodePtr& _node);

  /**
   * Add a definition to the net
   *
   * @param _node new definition
   */
  void add_member(const LnaNodePtr& _node);

  /**
   * Delete a definition from the net
   *
   * @param x definition to be deleted
   */
  void delete_member(const unsigned int& x);

  /**
   * Update a definition
   *
   * @param x definition to be modified
   * @param _node new definition
   */
  void update_member(const unsigned int& x, const LnaNodePtr& _node);

  /**
   * Get definition from the net
   *
   * @param x identifier of the definition
   * @return definition
   */
  LnaNodePtr get_member(const unsigned int& x) const;

  /**
   * Get the number of definitions
   *
   * @return size
   */
  size_t num_members() const;

 private:
  std::string name;
  std::vector<ParameterNodePtr> param_nodes;
};

/**
 * Type of pointers for NetNodes
 */
typedef std::shared_ptr<NetNode> NetNodePtr;

/**
 * @brief Class representing a structured net
 *
 * A net is described by a list of definitions that are the components of the
 * net. Elements that may be defined in a net are date types, constants,
 * functions, places, transitions, and state propositions
 */
class StructuredNetNode : public LnaNode {
 public:
  /**
   * Create a structured net
   */
  StructuredNetNode() : LnaNode(LnaNodeTypeStructuredNet) {}

  /**
   * Create a structured net
   *
   * @param _name  name of the net
   */
  StructuredNetNode(std::string _name)
      : LnaNode(LnaNodeTypeStructuredNet), name(_name) {}

  /**
   * Return the Helena code of the structured net
   *
   * @return helena code
   */
  std::string source_code();

  /**
   * Set the name of the net
   *
   * @param _name new name
   */
  void set_name(const std::string& _name);

  /**
   * Get the name of the net
   *
   * @return net's name
   */
  std::string get_name() const;

  /**
   * Add a net parameter
   *
   * @param _node new net parameter
   */
  void add_parameter(const ParameterNodePtr& _node);

  /**
   * Get a net parameter
   *
   * @param x identifier of the net parameter
   *
   * @return net parameter
   */
  ParameterNodePtr get_parameter(const unsigned int& x);

  /**
   * Get the number of net parameters
   *
   * @return  number of net parameters
   */
  size_t num_parameters() const;

  /**
   * Add color to the net
   *
   * @param _color color to be added
   */
  void add_color(const LnaNodePtr& _color);

  /**
   * Get a color from the net
   *
   * @param x identifier of the color
   * @return color
   */
  LnaNodePtr get_color(const unsigned int& x);

  /**
   * Get the number of colors in the net
   *
   * @return number of colors
   */
  size_t num_colors() const;

  /**
   * Add a place to the net
   *
   * @param _place place to be added
   */
  void add_place(const LnaNodePtr& _place);

  /**
   * Get a place from the net
   *
   * @param x identifier of the place
   * @return place
   */
  LnaNodePtr get_place(const unsigned int& x);

  /**
   * Get the number of places in the net
   *
   * @return number of places
   */
  size_t num_places() const;

  /**
   * Add a function to the net
   *
   * @param _function new function
   */
  void add_function(const LnaNodePtr& _function);

  /**
   * Get a function
   *
   * @param x identifier of the function
   * @return function
   */
  LnaNodePtr get_function(const unsigned int& x);

  /**
   * Get the number of functions in the net
   *
   * @return number of functions
   */
  size_t num_functions() const;

  /**
   * Add a transition to the net
   *
   * @param _transition transition to be added
   */
  void add_transition(const LnaNodePtr& _transition);

  /**
   * Get a transition from the net
   *
   * @param x identifier of the transition
   * @return transition
   */
  LnaNodePtr get_transition(const unsigned int& x);

  /**
   * Get the number of transitions in the net
   *
   * @return number of transitions
   */
  size_t num_transitions() const;

 private:
  std::string name;
  std::vector<ParameterNodePtr> param_nodes;
  std::vector<LnaNodePtr> color_nodes;
  std::vector<LnaNodePtr> place_nodes;
  std::vector<LnaNodePtr> function_nodes;
  std::vector<LnaNodePtr> transition_nodes;
};

/**
 * Type of pointers for StructuredNetNodes
 */
typedef std::shared_ptr<StructuredNetNode> StructuredNetNodePtr;

/**
 * @brief Class representing a comment
 *
 * Comments are indicated as in the C++ language.
 */
class CommentNode : public LnaNode {
 public:
  /**
   * Create a new comment
   */
  CommentNode() : LnaNode(LnaNodeTypeComment) {}

  /**
   * Create a new comment
   *
   * @param _comment content of the comment
   */
  CommentNode(const std::string& _comment)
      : comment(_comment), LnaNode(LnaNodeTypeComment) {}

  /**
   * Return the Helena code of a comment
   *
   * @return Helena code
   */
  std::string source_code();

  /**
   * Set content of the comment
   *
   * @param _comment content of the comment
   */
  void set_comment(const std::string& _comment);

  /**
   * Get content of the comment
   *
   * @return content
   */
  std::string get_comment() const;

 private:
  std::string comment;
};

/**
 * Type of pointers for CommentNodes
 */
typedef std::shared_ptr<CommentNode> CommentNodePtr;

/**
 * @brief Class representing a Color node
 *
 * Helena allows the definition of different kinds of data types: integer types
 * (range or modulo types), enumeration types, structured types, vector types,
 * and container types (list or set types). Enumeration and integer types form
 * the family of discrete types whereas other types are said to be composite.
 *
 * TODO: define classes for each type of color (range, modulo, ...)
 */
class ColorNode : public LnaNode {
 public:
  /**
   * Create a new color
   */
  ColorNode() : LnaNode(LnaNodeTypeColor) {}

  /**
   * Create a new color
   *
   * @param _node_type type of the new color
   */
  ColorNode(LnaNodeType _node_type) : LnaNode(_node_type) {}

  /**
   * Return the Helena code of the color
   *
   * @return helena code
   */
  std::string source_code();

  /**
   * Set the name of the color
   *
   * @param _name name of the color
   */
  void set_name(const std::string& _name);

  /**
   * Get the name of the color
   *
   * @return color's name
   */
  std::string get_name() const;

  /**
   * Set the type of the color
   *
   * @param _typeDef color's type
   */
  void set_typeDef(const std::string& _typeDef);

  /**
   * Get the type of the color
   *
   * @return color's type
   */
  std::string get_typeDef() const;

  /**
   * Set initial value of the color
   *
   * @param _value initial value
   */
  void set_init_value(const std::string& _value);

  /**
   * Get initial value of the color
   *
   * @return initial value
   */
  std::string get_init_value() const;

 protected:
  /**
   * Name of the color
   */
  std::string name;

  /**
   * Type of the color
   */
  std::string typeDef;

  /**
   * Initial value of the color
   */
  std::string init_value;
};

/**
 * Type of pointers for ColorNodes
 */
typedef std::shared_ptr<ColorNode> ColorNodePtr;

/**
 * @brief Class representing a subtype color
 *
 * Discrete types can be subtyped. Each subtype has a parent (which can also be
 * a subtype) and is defined by a constraint which limit the set of values which
 * belong to the subtype.
 *
 * A constraint simply consists of a range which must be statically evaluable
 * and which bounds must belong to the parent of the subtype.
 */
class SubColorNode : public ColorNode {
 public:
  /**
   * Create a new subtype color
   */
  SubColorNode() : ColorNode(LnaNodeTypeSub_Color) {}

  /**
   * Create a new subtype color
   *
   * @param _sub_color subtype
   */
  SubColorNode(ColorNodePtr _sub_color)
      : ColorNode(LnaNodeTypeSub_Color), subColor(_sub_color) {}

  /**
   * Return the Helena code of the subtype color
   *
   * @return helena code
   */
  std::string source_code();

  /**
   * Set the subtype
   *
   * @param _subcolor subtype
   */
  void set_subColor(const ColorNodePtr _subcolor);

  /**
   * Get subtype
   *
   * @return subtype
   */
  ColorNodePtr get_subColor() const;

 private:
  ColorNodePtr subColor;
};

/**
 * Type of pointers for SubColorNodes
 */
typedef std::shared_ptr<SubColorNode> SubColorNodePtr;

/**
 * @brief Class representing a component
 *
 * Elements of a structured type consist in contiguous elements called
 * components which may be of different types. Each component is identified by a
 * name.
 */
class ComponentNode : public LnaNode {
 public:
  /**
   * Create a new component
   */
  ComponentNode() : LnaNode(LnaNodeTypeComponent) {}

  /**
   * Create a new component
   *
   * @param _name name of the component
   * @param _type type of the component
   */
  ComponentNode(std::string _name, std::string _type)
      : LnaNode(LnaNodeTypeComponent), name(_name), type(_type) {}

  /**
   * Return the Helena code of the component
   *
   * @return helena code
   */
  std::string source_code();

  /**
   * Set the name of the component
   *
   * @param _name name of the component
   */
  void set_name(const std::string& _name);

  /**
   * Get the name of the component
   *
   * @return name of the component
   */
  std::string get_name() const;

  /**
   * Set the type of the component
   *
   * @param _type new type
   */
  void set_type(const std::string& _type);

  /**
   * Get the type of the component
   *
   * @return type
   */
  std::string get_type() const;

 private:
  std::string name;
  std::string type;
};

/**
 * Type of pointers for ComponentNodes
 */
typedef std::shared_ptr<ComponentNode> ComponentNodePtr;

/**
 * @brief Class representing a structured type
 *
 * Elements of a structured type consist in contiguous elements called
 * components which may be of different types. The number of elements in the
 * type is equal to the number of components in the declaration.
 */
class StructColorNode : public ColorNode {
 public:
  /**
   * Create a new structured type
   */
  StructColorNode() : ColorNode(LnaNodeTypeStruct_Color) {}

  /**
   * Return the Helena code of thestructured type
   *
   * @return helena code
   */
  std::string source_code();

  /**
   * Add a new component to the structured type
   *
   * @param _component component to be added
   */
  void add_component(const ComponentNodePtr& _component);

  /**
   * Get a component from the structured type
   *
   * @param x identifier of the component
   * @return component
   */
  ComponentNodePtr get_component(const unsigned int& x);

  /**
   * Get a component by its name
   *
   * @param _name component's name
   * @return component
   */
  ComponentNodePtr get_component_by_name(const std::string& _name);

  /**
   * Get the number of components
   *
   * @return number of components
   */
  size_t num_components() const;

 private:
  std::vector<ComponentNodePtr> components;
};

/**
 * Type of pointers for StructColorNodes
 */
typedef std::shared_ptr<StructColorNode> StructColorNodePtr;

/**
 * @brief Class representing a list type
 *
 * An element of a list type is a list which is defined as a finite sequence of
 * elements of the same type. The same item may appear several times in a list.
 * The elements in a list can be directly accessed via indexes. The capacity of
 * a list type is the maximal length of any list of this type. The expression
 * provided must be statically evaluable and strictly positive.
 */
class ListColorNode : public ColorNode {
 public:
  /**
   * Create a new list type
   */
  ListColorNode() : ColorNode(LnaNodeTypeListColor) {}

  /**
   * Return the Helena code of the list type
   *
   * @return helena code
   */
  std::string source_code();

  /**
   * Set the type of the index of the list
   *
   * @param _index_type index's type
   */
  void set_index_type(const std::string& _index_type);

  /**
   * Get the type of the index of the list
   *
   * @return index's type
   */
  std::string get_index_type() const;

  /**
   * Set the type of the elements of the list
   *
   * @param _element_type element's type
   */
  void set_element_type(const std::string& _element_type);

  /**
   * Get the type of the elements of the list
   *
   * @return element's type
   */
  std::string get_element_type() const;

  /**
   * Set the capacity of the list
   *
   * @param _capacity new capacity
   */
  void set_capacity(const std::string& _capacity);

  /**
   * Get the capacity of the list
   *
   * @return capacity
   */
  std::string get_capacity() const;

 private:
  std::string index_type;
  std::string element_type;
  std::string capacity;
};

/**
 * Type of pointers for ListColorNodes
 */
typedef std::shared_ptr<ListColorNode> ListColorNodePtr;

/**
 * @brief Class representing a constant value
 *
 * As in programming languages, constants may be defined at the net level. A
 * constant is defined by using the keyword `constant`. It must necessarily be
 * assigned a value which must have the type of the constant.
 */
class ConstantNode : public LnaNode {
 public:
  /**
   * Create a new constant
   */
  ConstantNode() : LnaNode(LnaNodeTypeConstant) {}

  /**
   * Return the Helena code of the constant
   *
   * @return helena code
   */
  std::string source_code();

  /**
   * Set the name of the constant
   *
   * @param _name new name
   */
  void set_name(const std::string& _name);

  /**
   * Get the name of the constant
   *
   * @return name
   */
  std::string get_name() const;

  /**
   * Set the type of the constant
   *
   * @param _type new type
   */
  void set_type(const std::string& _type);

  /**
   * Get the type of the constant
   *
   * @return constant's type
   */
  std::string get_type() const;

  /**
   * Set the expression of the constant
   *
   * @param _expression new expression
   */
  void set_expression(const std::string& _expression);

  /**
   * Get the expression of the constant
   *
   * @return expression
   */
  std::string get_expression() const;

 private:
  std::string name;
  std::string type;
  std::string expression;
};

/**
 * Type of pointers for ConstantNode
 */
typedef std::shared_ptr<ConstantNode> ConstantNodePtr;

/**
 * @brief Class representing a function parameter
 *
 * Functions take some parameters.
 */
class ParamNode : public LnaNode {
 public:
  /**
   * Create a new function parameter
   */
  ParamNode() : LnaNode(LnaNodeTypeParam) {}

  /**
   * Return the Helena code of the function parameter
   *
   * @return helena code
   */
  std::string source_code();

  /**
   * Set the name of the parameter
   * @param _name new name
   */
  void set_name(const std::string& _name);

  /**
   * Get the name of the parameter
   *
   * @return parameter's name
   */
  std::string get_name() const;

  /**
   * Set the type of the parameter
   *
   * @param _type new type
   */
  void set_type(const std::string& _type);

  /**
   * Get the type of the parameter
   *
   * @return parameter's type
   */
  std::string get_type() const;

 private:
  std::string name;
  std::string type;
};

/**
 * Type of pointers for ParamNode
 */
typedef std::shared_ptr<ParamNode> ParamNodePtr;

/**
 * @brief Class representing a function
 *
 * The user is allowed to define function which may then appear in arc
 * expressions or in the property to verify. Functions can not have any side
 * effect. They are functions in the mathematical sense: they take some
 * parameters, compute a value and return it.
 */
class FunctionNode : public LnaNode {
 public:
  /**
   * Create a new function
   */
  FunctionNode() : LnaNode(LnaNodeTypeFunc) {}

  /**
   * Return the Helena code of the function
   *
   * @return helena code
   */
  std::string source_code();

  /**
   * Set the name of the function
   *
   * @param _name function's name
   */
  void set_name(const std::string& _name);

  /**
   * Get the name of the function
   *
   * @return function's name
   */
  std::string get_name() const;

  /**
   * Add a parameter to the function
   *
   * @param _node new parameter
   */
  void add_parameter(const ParamNodePtr& _node);

  /**
   * Get a parameter of the function
   *
   * @param x identifier of the parameter
   * @return parameter
   */
  ParamNodePtr get_parameter(const unsigned int& x);

  /**
   * Set the return type of the function
   *
   * @param _returnType return type
   */
  void set_returnType(const std::string& _returnType);

  /**
   * Get the return type of the function
   *
   * @return type
   */
  std::string get_returnType() const;

  /**
   * Set the body of the function
   *
   * @param _body function's body
   */
  void set_body(const std::string& _body);

  /**
   * Get the body of the function
   *
   * @return function's body
   */
  std::string get_body() const;

 private:
  std::string name;
  std::string returnType;
  std::vector<ParamNodePtr> parameters_spec;
  std::string body;
};

/**
 * Type of pointers for FunctionNode
 */
typedef std::shared_ptr<FunctionNode> FunctionNodePtr;

/**
 * @brief Class representing a place of the net
 *
 * The state of a system modeled by a Petri net is given by the distribution
 * (or marking) of items called tokens upon the places of the net. In high-level
 * nets, these tokens are typed (i.e. the domain of the place).
 *
 * We call capacity of a place, the maximal multiplicity (i.e., number of
 * repetitions of a token) of any item in the place.
 */
class PlaceNode : public LnaNode {
 public:
  /**
   * Create a new place
   */
  PlaceNode() : LnaNode(LnaNodeTypePlace) {}

  /**
   * Return the Helena code of the place
   *
   * @return helena code
   */
  std::string source_code();

  /**
   * Set the name of the place
   *
   * @param _name place's name
   */
  void set_name(const std::string& _name);

  /**
   * Get the name of the place
   *
   * @return  place's name
   */
  std::string get_name() const;

  /**
   * Set the domain of the place
   *
   * @param _domain place's domain
   */
  void set_domain(const std::string& _domain);

  /**
   * Get the domain of the place
   *
   * @return place's domain
   */
  std::string get_domain() const;

  /**
   * Set the initial marking of the place
   *
   * @param _init initial marking
   */
  void set_init(const std::string& _init);

  /**
   * Get the initial marking of the place
   *
   * @return initial marking
   */
  std::string get_init() const;

  /**
   * Set the capacity of the place
   *
   * @param _capacity new capacity
   */
  void set_capacity(const std::string& _capacity);

  /**
   * Get the capacity of the place
   *
   * @return place's capacity
   */
  std::string get_capacity() const;

  /**
   * Set the type of the place
   *
   * @param _type place's type
   */
  void set_type(const std::string& _type);

  /**
   * Get the type of the place
   *
   * @return place's type
   */
  std::string get_type() const;

 private:
  std::string name;
  std::string domain;
  std::string init;
  std::string capacity;
  std::string type;
};

/**
 * Type of pointers for PlaceNode
 */
typedef std::shared_ptr<PlaceNode> PlaceNodePtr;

/**
 * @brief Class representing an arc in the net
 *
 * An arc is characterized by the place from which we remove, add or check
 * tokens, and by an expression specifying for a given instantiation of the
 * variable of the transition the considered tokens.
 */
class ArcNode : public LnaNode {
 public:
  /**
   * Create a new arc
   */
  ArcNode() : LnaNode(LnaNodeTypeArc) {}

  /**
   * Return the Helena code of the arc
   *
   * @return helena code
   */
  std::string source_code();

  /**
   * Set the place of the arc
   *
   * @param _placeName place's name
   */
  void set_placeName(const std::string& _placeName);

  /**
   * Get the place of the arc
   *
   * @return  place's name
   */
  std::string get_placeName() const;

  /**
   * Set the label of the arc
   *
   * @param _label arc's label
   */
  void set_label(const std::string& _label);

  /**
   * Get the label of the arc
   *
   * @return arc's label
   */
  std::string get_label() const;

 private:
  std::string placeName;
  std::string label;
};

/**
 * Type of pointers for ArcNode
 */
typedef std::shared_ptr<ArcNode> ArcNodePtr;

/**
 * @brief Class representing a transition in the net
 *
 * Transitions of a Petri net are active nodes that may change the state of the
 * system (i.e., the distribution of tokens in the places). Transitions need
 * some tokens in their input places to be firable and produce tokens in their
 * output places.
 *
 * To further restrain the firability of a transition, inhibitor
 * arcs may be used to specify that some tokens must not be present in a
 * specific place. In high-level Petri nets, arcs between places and transitions
 * are labeled by expressions in which variables appear. Thus, a transition is
 * firable for a given instantatiation (or binding) os these variables.
 *
 * Transitions in Helena are identified by a name. The description of a
 * transition must specify the input and output plaxes of the transition
 * followed by inhibitor arcs (if any), free variables (if any), bound variables
 * (if any), and finally its attributes: a guard, a priority, a description and
 * a safe attribute.
 */
class TransitionNode : public LnaNode {
 public:
  /**
   * Create a new transition
   */
  TransitionNode() : LnaNode(LnaNodeTypeTransition) {}

  /**
   * Return the Helena code of the transition
   *
   * @return helena code
   */
  std::string source_code();

  /**
   * Set the name of the transition
   *
   * @param _name name
   */
  void set_name(const std::string& _name);

  /**
   * Get the name of the transition
   *
   * @return transition's name
   */
  std::string get_name() const;

  /**
   * Add an ingoing arc
   *
   * @param _node arc to be added
   */
  void add_inArc(const ArcNodePtr& _node);

  /**
   * Get an ingoing arc by its name
   *
   * @param _name arc's name
   * @return arc
   */
  ArcNodePtr get_in_arc_by_name(const std::string& _name);

  /**
   * Add an outgoing arc
   *
   * @param _node arc to be added
   */
  void add_outArc(const ArcNodePtr& _node);

  /**
   * Get an outgoing arc by its name
   *
   * @param _name arc's name
   * @return  arc
   */
  ArcNodePtr get_out_arc_by_name(const std::string& _name);

  /**
   * Add an inhibitor arc to the transition
   *
   * @param _node arc to be added
   */
  void add_inhibitArc(const ArcNodePtr& _node);

  /**
   * Set the guard of the transition
   *
   * @param _guard guard
   */
  void set_guard(const std::string& _guard);

  /**
   * Get the guard of the transition
   *
   * @return guard
   */
  std::string get_guard() const;

  /**
   * Set the priority of the transition
   *
   * @param _priority new priority
   */
  void set_priority(const std::string& _priority);

  /**
   * Get the priority of the transition
   *
   * @return prioritiy
   */
  std::string get_priority() const;

  /**
   * Set the description of the transition
   *
   * @param _description new description
   */
  void set_description(const std::string& _description);

  /**
   * Get the description of the transition
   *
   * @return description
   */
  std::string get_description() const;

  /**
   * Set the safe attribute of the transition
   *
   * @param _safe safe attribute
   */
  void set_safe(const std::string& _safe);

  /**
   * Get the safe attribute of the transition
   *
   * @return safe attribute
   */
  std::string get_safe() const;

  /**
   * Add a new bounded variable to the transition
   *
   * @param _node  varibles
   */
  void add_let(const std::string& _node);

 private:
  std::string name;
  std::vector<ArcNodePtr> inArcs;
  std::vector<ArcNodePtr> outArcs;
  std::vector<ArcNodePtr> inhibitArcs;
  std::vector<std::string> lets;
  std::string guard;
  std::string priority;
  std::string description;
  std::string safe;
};

/**
 * Type of pointers for a TransitionNode
 */
typedef std::shared_ptr<TransitionNode> TransitionNodePtr;

}  // namespace HELENA

#endif  // HELENA_H_
