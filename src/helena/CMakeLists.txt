cmake_minimum_required(VERSION 3.13)

# project information
project(helena CXX)

# Source folder
add_subdirectory(src)