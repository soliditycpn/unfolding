# Authors

**unfolding** has been mainly developed by:

- Jaime Arias (CNRS, LIPN, Université Sorbonne Paris Nord)
- Ikram Garfatta (LIPN, Université Sorbonne Paris Nord)
- Quy Ban Tran (LIPN, Université Sorbonne Paris Nord)

with contributions from:

- Kaïs Klai (LIPN, Université Sorbonne Paris Nord)
